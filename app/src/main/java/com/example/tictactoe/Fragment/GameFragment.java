package com.example.tictactoe.Fragment;

import android.annotation.SuppressLint;
import android.graphics.Bitmap;
import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.tictactoe.R;
import com.example.tictactoe.ChessboardAdapter;

import java.util.ArrayList;

public class GameFragment extends Fragment {

    public static String TAG = GameFragment.class.getName();
    public static boolean turnO = true;
    @SuppressLint("StaticFieldLeak")
    public static TextView txt_turn, win_x, win_o, txt_win, btn_reset;
    @SuppressLint("StaticFieldLeak")
    public static ImageView img_stroke, img_win;
    @SuppressLint("StaticFieldLeak")
    public static RelativeLayout rl_win;
    private ChessboardAdapter chessboardAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_game, container, false);
        RecyclerView rv_table = view.findViewById(R.id.rv_chessboard);
        txt_turn = view.findViewById(R.id.txt_turn);
        txt_win = view.findViewById(R.id.txt_win);
        img_stroke = view.findViewById(R.id.img_stroke);
        rl_win = view.findViewById(R.id.rl_win);
        img_win = view.findViewById(R.id.img_win);
        win_x = view.findViewById(R.id.win_x);
        win_o = view.findViewById(R.id.win_o);
        btn_reset = view.findViewById(R.id.btn_reset);
        ImageView img_back = view.findViewById(R.id.img_back);
        Button btn_again = view.findViewById(R.id.btn_again);
        Button btn_home = view.findViewById(R.id.btn_home);
        ArrayList<Bitmap> arrBms = new ArrayList<>();
        for (int i = 0; i < 9; i++){
            arrBms.add(null);
        }
        chessboardAdapter = new ChessboardAdapter(requireContext(), arrBms);
        RecyclerView.LayoutManager layoutManager = new GridLayoutManager(getContext(), 3);
        rv_table.setLayoutManager(layoutManager);
        rv_table.setAdapter(chessboardAdapter);
        btn_reset.setOnClickListener(v -> reset());
        btn_again.setOnClickListener(v -> {
            rl_win.setVisibility(View.INVISIBLE);
            reset();
        });
        btn_home.setOnClickListener(v -> {
            reset();
            assert getFragmentManager() != null;
            getFragmentManager().popBackStack();
        });
        img_back .setOnClickListener(view1 -> {
            reset();
            assert getFragmentManager() != null;
            getFragmentManager().popBackStack();
        });
        return view;
    }
    @SuppressLint("NotifyDataSetChanged")
    private void reset() {
        ArrayList<Bitmap> arrBms = new ArrayList<>();
        for (int i = 0; i < 9; i++){
            arrBms.add(null);
        }
        img_stroke.setImageBitmap(null);
        chessboardAdapter.setArrBms(arrBms);
        chessboardAdapter.notifyDataSetChanged();
        turnO = true;
        txt_turn.setText(requireContext().getString(R.string.turn_of_o));
    }
}