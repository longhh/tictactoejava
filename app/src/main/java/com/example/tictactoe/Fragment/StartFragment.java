package com.example.tictactoe.Fragment;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.example.tictactoe.MainActivity;
import com.example.tictactoe.R;
import java.util.Objects;

public class StartFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start, container, false);
        TextView r_play_with_computer = view.findViewById(R.id.btn_player_with_computer);
        TextView r_2_player = view.findViewById(R.id.btn_player_with_player);
        r_play_with_computer.setOnClickListener(v -> {
            MainActivity.multiPlayer = false;
            MainActivity.scoreO = 0;
            MainActivity.scoreX = 0;
            FragmentTransaction transaction = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
            transaction.addToBackStack(GameFragment.TAG);
            transaction.replace(R.id.frame_main, new GameFragment());
            transaction.commit();
        });
        r_2_player.setOnClickListener(v -> {
            MainActivity.multiPlayer = true;
            MainActivity.scoreO = 0;
            MainActivity.scoreX = 0;
            FragmentTransaction transaction = Objects.requireNonNull(getActivity()).getSupportFragmentManager().beginTransaction();
            transaction.addToBackStack(GameFragment.TAG);
            transaction.replace(R.id.frame_main, new GameFragment());
            transaction.commit();
        });

        return view;
    }
}