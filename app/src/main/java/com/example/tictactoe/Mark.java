package com.example.tictactoe;

public class Mark {
    private int position;
    private int point;

    public Mark(int position, int point) {
        this.position = position;
        this.point = point;
    }

    public int getPosition() {
        return position;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    public int getPoint() {
        return point;
    }

    public void setPoint(int point) {
        this.point = point;
    }

    @Override
    public String toString() {
        return "{" + position +", " + point +"}";
    }
}
