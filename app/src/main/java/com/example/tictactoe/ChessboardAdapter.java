package com.example.tictactoe;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import com.example.tictactoe.Fragment.GameFragment;
import java.util.ArrayList;

public class ChessboardAdapter extends RecyclerView.Adapter<ChessboardAdapter.ChessBoardViewHolder> {
    private final Context context;
    private ArrayList<Bitmap> arrBms, arrBmTest;
    private final Bitmap bitmapO, bitmapX, draw;
    private final ArrayList<Bitmap> arrStroke;
    private String winCharacter = "o";
    private Animation anim_x_o, animStroke, animWin;

    public ChessboardAdapter(Context context, ArrayList<Bitmap> arrBms) {
        this.context = context;
        this.arrBms = arrBms;
        bitmapO = BitmapFactory.decodeResource(context.getResources(), R.drawable.o);
        bitmapX = BitmapFactory.decodeResource(context.getResources(), R.drawable.x);
        draw = BitmapFactory.decodeResource(context.getResources(), R.drawable.xo);
        arrStroke = new ArrayList<>();
        arrStroke.add(BitmapFactory.decodeResource(context.getResources(), R.drawable.stroke1));
        arrStroke.add(BitmapFactory.decodeResource(context.getResources(), R.drawable.stroke2));
        arrStroke.add(BitmapFactory.decodeResource(context.getResources(), R.drawable.stroke3));
        arrStroke.add(BitmapFactory.decodeResource(context.getResources(), R.drawable.stroke4));
        arrStroke.add(BitmapFactory.decodeResource(context.getResources(), R.drawable.stroke5));
        arrStroke.add(BitmapFactory.decodeResource(context.getResources(), R.drawable.stroke6));
        arrStroke.add(BitmapFactory.decodeResource(context.getResources(), R.drawable.stroke7));
        arrStroke.add(BitmapFactory.decodeResource(context.getResources(), R.drawable.stroke8));
    }

    @NonNull
    @Override
    public ChessBoardViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ChessBoardViewHolder(LayoutInflater.from(context).inflate(R.layout.item_table, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull final ChessBoardViewHolder holder, int position) {
        holder.img_item_table.setImageBitmap(arrBms.get(position));
        anim_x_o = AnimationUtils.loadAnimation(context, R.anim.anim_x_o);
        holder.img_item_table.setAnimation(anim_x_o);
        animStroke = AnimationUtils.loadAnimation(context, R.anim.anim_stroke);
        GameFragment.img_stroke.setAnimation(animStroke);
        animWin = AnimationUtils.loadAnimation(context, R.anim.anim_stroke);

        if (MainActivity.multiPlayer) {
            playWith2Player(holder, position);
        } else {
            playWithComputer(holder, position);
        }
        if (!checkWin()) checkDraw();
    }

    private void checkDraw() {
        int count = 0;
        for (int i = 0; i < arrBms.size(); i++) {
            if (arrBms.get(i) != null) {
                count++;
            }
        }
        if (count == 9) {
            GameFragment.img_stroke.startAnimation(animStroke);
            GameFragment.rl_win.setVisibility(View.VISIBLE);
            GameFragment.rl_win.setAnimation(animWin);
            GameFragment.rl_win.startAnimation(animWin);
            GameFragment.img_win.setImageBitmap(draw);
            GameFragment.txt_win.setText(context.getString(R.string.draw));
        }
    }

    private void playWith2Player(ChessBoardViewHolder holder, int position) {
        holder.img_item_table.setOnClickListener(v -> {
            if (arrBms.get(position) == null && !checkWin()) {
                if (GameFragment.turnO) {
                    arrBms.set(position, bitmapO);
                    GameFragment.turnO = false;
                    GameFragment.txt_turn.setText(context.getString(R.string.turn_of_x));
                } else {
                    arrBms.set(position, bitmapX);
                    GameFragment.turnO = true;
                    GameFragment.txt_turn.setText(context.getString(R.string.turn_of_o));
                }
                holder.img_item_table.startAnimation(anim_x_o);
                if (checkWin()) {
                    win();
                }
                notifyItemChanged(position);
            }
        });
    }

    private void playWithComputer(ChessBoardViewHolder holder, int position) {
        holder.img_item_table.setOnClickListener(v -> {
            if (arrBms.get(position) == null && !checkWin() && GameFragment.turnO) {
                arrBms.set(position, bitmapO);
                GameFragment.turnO = false;
                GameFragment.txt_turn.setText(context.getString(R.string.turn_of_x));
                holder.img_item_table.startAnimation(anim_x_o);
                if (checkWin()) {
                    win();
                }
                notifyItemChanged(position);
                Handler handler = new Handler();
                Runnable runnable = () -> {
                    arrBmTest = arrBms;
                    ArrayList<Mark> arrMark = solver(bitmapX);
                    if (arrMark.size() > 0) {
                        int max = arrMark.get(0).getPoint();
                        int pos = 0;
                        for (int i = 0; i < arrMark.size(); i++) {
                            if (max < arrMark.get(i).getPoint()) {
                                max = arrMark.get(i).getPoint();
                                pos = i;
                            }
                        }
                        arrBms.set(arrMark.get(pos).getPosition(), bitmapX);
                        if (checkWin()) {
                            win();
                        } else {
                            GameFragment.turnO = true;
                            GameFragment.txt_turn.setText(context.getString(R.string.turn_of_o));
                        }
                        notifyItemChanged(arrMark.get(pos).getPosition());
                    }
                };
                if (!checkWin()) {
                    handler.postDelayed(runnable, 1000);
                }
            }
        });
    }

    @NonNull
    private ArrayList<Mark> solver(Bitmap bm) {
        ArrayList<Mark> arrPoints = new ArrayList<>();
        for (int i = 0; i < 9; i++) {
            if (arrBmTest.get(i) == null) {
                if (bm == bitmapX) {
                    arrBmTest.set(i, bitmapX);
                } else {
                    arrBmTest.set(i, bitmapO);
                }
                if (checkWinTmp(bm) == -1) {
                    if (bm == bitmapX) {
                        ArrayList<Mark> arr = solver(bitmapO);
                        int minimum = 5;
                        for (int j = 0; j < arr.size(); j++) {
                            if (minimum > arr.get(j).getPoint()) {
                                minimum = arr.get(j).getPoint();
                            }
                        }
                        if (minimum != 5) {
                            arrPoints.add(new Mark(i, minimum));
                        }
                    } else {
                        ArrayList<Mark> arr = solver(bitmapX);
                        int maximum = -5;
                        for (int j = 0; j < arr.size(); j++) {
                            if (maximum < arr.get(j).getPoint()) {
                                maximum = arr.get(j).getPoint();
                            }
                        }
                        if (maximum != -5) {
                            arrPoints.add(new Mark(i, maximum));
                        }
                    }
                } else {
                    if (bm == bitmapX) {
                        arrPoints.add(new Mark(i, checkWinTmp(bm)));
                    } else {
                        arrPoints.add(new Mark(i, -(checkWinTmp(bm))));
                    }
                }
                arrBmTest.set(i, null);
            }
        }
        return arrPoints;
    }

    private int checkWinTmp(Bitmap bm) {
        //check row
        int countRow = 0;
        for (int i = 0; i < 9; i++) {
            if (i % 3 == 0) {
                countRow = 0;
            }
            if (arrBmTest.get(i) == bm) {
                countRow++;
            }
            if (countRow == 3) {
                return 1;
            }
        }
        //check column
        if (arrBmTest.get(0) == arrBmTest.get(3) && arrBmTest.get(3) == arrBmTest.get(6) && arrBmTest.get(0) == bm
                || arrBmTest.get(1) == arrBmTest.get(4) && arrBmTest.get(4) == arrBmTest.get(7) && arrBmTest.get(1) == bm
                || arrBmTest.get(2) == arrBmTest.get(5) && arrBmTest.get(5) == arrBmTest.get(8) && arrBmTest.get(2) == bm) {
            return 1;
        }
        //check diagonal
        if (arrBmTest.get(0) == arrBmTest.get(4) && arrBmTest.get(4) == arrBmTest.get(8) && arrBmTest.get(0) == bm)
            return 1;
        if (arrBmTest.get(2) == arrBmTest.get(4) && arrBmTest.get(4) == arrBmTest.get(6) && arrBmTest.get(2) == bm)
            return 1;

        int count = 0;
        for (int i = 0; i < 9; i++) {
            if (arrBmTest.get(i) != null) {
                count++;
            }
        }
        if (count == 9) {
            return 0;
        }
        return -1;
    }

    @SuppressLint("SetTextI18n")
    private void win() {
        GameFragment.img_stroke.startAnimation(animWin);
        GameFragment.rl_win.setVisibility(View.VISIBLE);
        GameFragment.rl_win.setAnimation(animWin);
        GameFragment.rl_win.startAnimation(animWin);
        if (winCharacter.equals("o")) {
            GameFragment.img_win.setImageBitmap(bitmapO);
            MainActivity.scoreO++;
            GameFragment.win_o.setText(context.getString(R.string.score_o) + " " + MainActivity.scoreO);
        } else {
            GameFragment.img_win.setImageBitmap(bitmapX);
            MainActivity.scoreX++;
            GameFragment.win_x.setText(context.getString(R.string.score_x) + " " + MainActivity.scoreX);
        }
        GameFragment.txt_win.setText(context.getString(R.string.win));
    }

    private boolean checkWin() {
        if (arrBms.get(0) == arrBms.get(3) && arrBms.get(3) == arrBms.get(6) && arrBms.get(0) != null) {
            GameFragment.img_stroke.setImageBitmap(arrStroke.get(2));
            checkWinCharacter(0);
            return true;
        } else if (arrBms.get(1) == arrBms.get(4) && arrBms.get(4) == arrBms.get(7) && arrBms.get(1) != null) {
            GameFragment.img_stroke.setImageBitmap(arrStroke.get(3));
            checkWinCharacter(1);
            return true;
        } else if (arrBms.get(2) == arrBms.get(5) && arrBms.get(5) == arrBms.get(8) && arrBms.get(2) != null) {
            GameFragment.img_stroke.setImageBitmap(arrStroke.get(4));
            checkWinCharacter(2);
            return true;
        } else if (arrBms.get(0) == arrBms.get(1) && arrBms.get(1) == arrBms.get(2) && arrBms.get(0) != null) {
            GameFragment.img_stroke.setImageBitmap(arrStroke.get(5));
            checkWinCharacter(0);
            return true;
        } else if (arrBms.get(3) == arrBms.get(4) && arrBms.get(4) == arrBms.get(5) && arrBms.get(3) != null) {
            GameFragment.img_stroke.setImageBitmap(arrStroke.get(6));
            checkWinCharacter(3);
            return true;
        } else if (arrBms.get(6) == arrBms.get(7) && arrBms.get(7) == arrBms.get(8) && arrBms.get(6) != null) {
            GameFragment.img_stroke.setImageBitmap(arrStroke.get(7));
            checkWinCharacter(6);
            return true;
        } else if (arrBms.get(0) == arrBms.get(4) && arrBms.get(4) == arrBms.get(8) && arrBms.get(0) != null) {
            GameFragment.img_stroke.setImageBitmap(arrStroke.get(1));
            checkWinCharacter(0);
            return true;
        } else if (arrBms.get(2) == arrBms.get(4) && arrBms.get(4) == arrBms.get(6) && arrBms.get(2) != null) {
            GameFragment.img_stroke.setImageBitmap(arrStroke.get(0));
            checkWinCharacter(2);
            return true;
        }
        return false;
    }

    private void checkWinCharacter(int i) {
        if (arrBms.get(i) == bitmapO) {
            winCharacter = "o";
        } else {
            winCharacter = "x";
        }
    }

    @Override
    public int getItemCount() {
        return arrBms.size();
    }

    static class ChessBoardViewHolder extends RecyclerView.ViewHolder {

        private final ImageView img_item_table;

        public ChessBoardViewHolder(@NonNull View itemView) {
            super(itemView);
            img_item_table = itemView.findViewById(R.id.img_item_table);
        }
    }

    public void setArrBms(ArrayList<Bitmap> arrBms) {
        this.arrBms = arrBms;
    }
}
